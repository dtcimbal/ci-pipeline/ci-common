# CI Common

## Description

This project is a collection of common files for GitLab CI/CD pipelines. These files are intended to be used across multiple projects 
to ensure consistency, reduce duplication, and simplify maintenance CI/CD processes. 

## Table of Contents

- [Features](#features)
- [Usage](#usage)
- [File Structure](#file-structure)
- [Contributing](#contributing)
- [License](#license)

## Features

- Provides pre-configured GitLab CI/CD pipeline templates.
- Includes common stages such as dev and production and the rules when the stages applies.
- Enables easy customization and extension for specific project requirements.
- Facilitates rapid setup and deployment of CI/CD pipelines.

## Usage

To use these common files in your GitLab project:

1. Include `remote: https://gitlab.com/dtcimbal/ci-pipeline/ci-common/.gitlab-ci/<template>.yml` to the `gitlab-ci.yml` in your repository.
2. Customize the tasks within your `gitlab-ci.yml` using stages and job templates defined in `ci-common`.
3. Commit the changes and push them to your GitLab repository.

## File Structure

The file structure of this repository is organized as follows:

```
project-common-files/
├── .gitlab-ci/
│ ├── image-tf.yml # Terraform job template
│ └── stages.yml # GitLab CI/CD common stages and rules
├── .gitignore # Git ignore rules
├── LICENSE # License information
└── README.md # Project README (you're reading it!)
```

## Contributing

Contributions to enhance and expand these common files are welcome! If you have suggestions, improvements, or new features to propose, please follow these steps:

1. Fork this repository.
2. Create a new branch for your feature (`git checkout -b feature/your-feature`).
3. Make your changes and commit them (`git commit -am 'Add new feature'`).
4. Push to the branch (`git push origin feature/your-feature`).
5. Create a new Pull Request and describe your changes in detail.

Please ensure that your contributions align with the project's goals and follow the existing coding conventions and standards.

## License

This project is licensed under the [MIT License](LICENSE). You are free to modify and distribute the code for both commercial and non-commercial purposes. See the [LICENSE](LICENSE) file for more details.